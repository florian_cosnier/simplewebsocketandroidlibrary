/******************************************************************************
*
*  Copyright 2014 Florian COSNIER
*  Copyright 2011-2012 Tavendo GmbH
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*  
*  Code based on the library AutobahnAndroid BY Tavendo GmbH
*  http://autobahn.ws/android
*
******************************************************************************/

package fr.aws.library;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.SocketChannel;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

public class WSConnection implements WS {

	   private static final String TAG = "WSConnection";

	   protected Handler mMasterHandler;

	   protected WSReader mReader;
	   protected WSWriter mWriter;
	   protected HandlerThread mWriterThread;

	   protected SocketChannel mTransportChannel;

	   private URI mWsUri;
	   private String mWsScheme;
	   private String mWsHost;
	   private int mWsPort;
	   private String mWsPath;
	   private String mWsQuery;
	   private String[] mWsSubprotocols;
	   private List<BasicNameValuePair> mWsHeaders;

	   private WS.ConnectionHandler mWsHandler;

	   protected WSOptions mOptions;
	   
	   private boolean mActive;
	   private boolean mPrevConnected;

	        /**
	         * Asynchronous socket connector.
	         */
	        private class WSConnector extends Thread {

	                public void run() {
	                        Thread.currentThread().setName("WSConnector");

	                        /*
	                         * connect TCP socket
	                         */
	                        try {
	                                mTransportChannel = SocketChannel.open();

	                                // the following will block until connection was established or
	                                // an error occurred!
	                                mTransportChannel.socket().connect(
	                                                new InetSocketAddress(mWsHost, mWsPort),
	                                                mOptions.getSocketConnectTimeout());

	                                // before doing any data transfer on the socket, set socket
	                                // options
	                                mTransportChannel.socket().setSoTimeout(
	                                                mOptions.getSocketReceiveTimeout());
	                                mTransportChannel.socket().setTcpNoDelay(
	                                                mOptions.getTcpNoDelay());

	                        } catch (IOException e) {
	                                onClose(WSConnectionHandler.CLOSE_CANNOT_CONNECT,
	                                                e.getMessage());
	                                return;
	                        }

	                        if (mTransportChannel.isConnected()) {

	                                try {

	                                        // create & start WebSocket reader
	                                        createReader();

	                                        // create & start WebSocket writer
	                                        createWriter();

	                                        // start WebSockets handshake
	                                        WSMessage.ClientHandshake hs = new WSMessage.ClientHandshake(
	                                                        mWsHost + ":" + mWsPort);
	                                        hs.mPath = mWsPath;
	                                        hs.mQuery = mWsQuery;
	                                        hs.mSubprotocols = mWsSubprotocols;
	                                        hs.mHeaderList = mWsHeaders;
	                                        mWriter.forward(hs);

	                                        mPrevConnected = true;

	                                } catch (Exception e) {
	                                        onClose(WSConnectionHandler.CLOSE_INTERNAL_ERROR,
	                                                        e.getMessage());
	                                        return;
	                                }
	                        } else {
	                                onClose(WSConnectionHandler.CLOSE_CANNOT_CONNECT,
	                                                "Could not connect to WebSocket server");
	                                return;
	                        }
	                }

	        }

	   public WSConnection() {
	      Log.d(TAG, "Created");
	      
	      // create WebSocket master handler
	      createHandler();
	      
	      // set initial values
	      mActive = false;
	      mPrevConnected = false;
	   }


	   public void sendTextMessage(String payload) {
	      mWriter.forward(new WSMessage.TextMessage(payload));
	   }


	  /* public void sendRawTextMessage(byte[] payload) {
	      mWriter.forward(new WSMessage.RawTextMessage(payload));
	   }


	   public void sendBinaryMessage(byte[] payload) {
	      mWriter.forward(new WSMessage.BinaryMessage(payload));
	   }*/


	   public boolean isConnected() {
	      return mTransportChannel != null && mTransportChannel.isConnected();
	   }


	   private void failConnection(int code, String reason) {

	      Log.d(TAG, "fail connection [code = " + code + ", reason = " + reason);

	      if (mReader != null) {
	         mReader.quit();
	         try {
	            mReader.join();
	         } catch (InterruptedException e) {
	            e.printStackTrace();
	         }
	         //mReader = null;
	      } else {
	         Log.d(TAG, "mReader already NULL");
	      }

	      if (mWriter != null) {
	         //mWriterThread.getLooper().quit();
	         mWriter.forward(new WSMessage.Quit());
	         try {
	            mWriterThread.join();
	         } catch (InterruptedException e) {
	            e.printStackTrace();
	         }
	         //mWriterThread = null;
	      } else {
	         Log.d(TAG, "mWriter already NULL");
	      }

	      if (mTransportChannel != null) {
	         try {
	            mTransportChannel.close();
	         } catch (IOException e) {
	            e.printStackTrace();
	         }
	         //mTransportChannel = null;
	      } else {
	         Log.d(TAG, "mTransportChannel already NULL");
	      }

	      onClose(code, reason);

	      Log.d(TAG, "worker threads stopped");
	   }


	   public void connect(String wsUri, WS.ConnectionHandler wsHandler) throws WSException {
	      connect(wsUri, wsHandler, new WSOptions(), null);
	   }


	   public void connect(String wsUri, WS.ConnectionHandler wsHandler, WSOptions options) throws WSException {
	      connect(wsUri, wsHandler, options, null);
	   }


	   public void connect(String wsUri, WS.ConnectionHandler wsHandler, WSOptions options, List<BasicNameValuePair> headers) throws WSException {

	      // don't connect if already connected .. user needs to disconnect first
	      //
	      if (mTransportChannel != null && mTransportChannel.isConnected()) {
	         throw new WSException("already connected");
	      }

	      // parse WebSockets URI
	      //
	      try {
	         mWsUri = new URI(wsUri);

	         if (!mWsUri.getScheme().equals("ws") && !mWsUri.getScheme().equals("wss")) {
	            throw new WSException("unsupported scheme for WebSockets URI");
	         }

	         if (mWsUri.getScheme().equals("wss")) {
	            throw new WSException("secure WebSockets not implemented");
	         }

	         mWsScheme = mWsUri.getScheme();

	         if (mWsUri.getPort() == -1) {
	            if (mWsScheme.equals("ws")) {
	               mWsPort = 80;
	            } else {
	               mWsPort = 443;
	            }
	         } else {
	            mWsPort = mWsUri.getPort();
	         }

	         if (mWsUri.getHost() == null) {
	            throw new WSException("no host specified in WebSockets URI");
	         } else {
	            mWsHost = mWsUri.getHost();
	         }

	         if (mWsUri.getPath() == null || mWsUri.getPath().equals("")) {
	            mWsPath = "/";
	         } else {
	            mWsPath = mWsUri.getPath();
	         }

	         if (mWsUri.getQuery() == null || mWsUri.getQuery().equals("")) {
	            mWsQuery = null;
	         } else {
	            mWsQuery = mWsUri.getQuery();
	         }

	      } catch (URISyntaxException e) {

	         throw new WSException("invalid WebSockets URI");
	      }

	      mWsHeaders = headers;
	      mWsHandler = wsHandler;

	      // make copy of options!
	      mOptions = new WSOptions(options);
	      
	      // set connection active
	      mActive = true;

	      // use asynch connector on short-lived background thread
	      new WSConnector().start();
	   }


	   public void disconnect() {
	      if (mWriter != null) {
	         mWriter.forward(new WSMessage.Close(1000));
	      } else {
	         Log.d(TAG, "could not send Close .. writer already NULL");
	      }
	      mActive = false;
	      mPrevConnected = false;
	   }
	   
	   /**
	    * Reconnect to the server with the latest options 
	    * @return true if reconnection performed
	    */
	   public boolean reconnect() {
	           if (!isConnected() && (mWsUri != null)) {
	                   new WSConnector().start();
	                   return true;
	           }
	           return false;
	   }
	   
	   /**
	    * Perform reconnection
	    * 
	    * @return true if reconnection was scheduled
	    */
	   protected boolean scheduleReconnect() {
	           /**
	            * Reconnect only if:
	            *  - connection active (connected but not disconnected)
	            *  - has previous success connections
	            *  - reconnect interval is set
	            */
	           int interval = mOptions.getReconnectInterval();
	           boolean need = mActive && mPrevConnected && (interval > 0);
	           if (need) {
	                   Log.d(TAG, "Reconnection scheduled");
	                   mMasterHandler.postDelayed(new Runnable() {
	                        
	                        public void run() {
	                                Log.d(TAG, "Reconnecting...");
	                                reconnect();
	                        }
	                }, interval);
	           }
	           return need;
	   }
	   
	   /**
	    * Common close handler
	    * 
	    * @param code       Close code.
	        * @param reason     Close reason (human-readable).
	    */
	   private void onClose(int code, String reason) {
	           boolean reconnecting = false;
	           
	           if ((code == WS.ConnectionHandler.CLOSE_CANNOT_CONNECT) ||
	                           (code == WS.ConnectionHandler.CLOSE_CONNECTION_LOST)) {
	                   reconnecting = scheduleReconnect();
	           }
	           
	           
	           if (mWsHandler != null) {
	                   try {
	                           if (reconnecting) {
	                                   mWsHandler.onClose(WS.ConnectionHandler.CLOSE_RECONNECT, reason);
	                           } else {
	                                   mWsHandler.onClose(code, reason);
	                           }
	                   } catch (Exception e) {
	                           e.printStackTrace();
	                   }
	                   //mWsHandler = null;
	           } else {
	                   Log.d(TAG, "mWsHandler already NULL");
	           }
	   }


	   /**
	    * Create master message handler.
	    */
	   protected void createHandler() {

	      mMasterHandler = new Handler() {

	         public void handleMessage(Message msg) {

	            if (msg.obj instanceof WSMessage.TextMessage) {

	               WSMessage.TextMessage textMessage = (WSMessage.TextMessage) msg.obj;

	               if (mWsHandler != null) {
	                  mWsHandler.onTextMessage(textMessage.mPayload);
	               } else {
	                  Log.d(TAG, "could not call onTextMessage() .. handler already NULL");
	               }

	            } 
	            /*else if (msg.obj instanceof WSMessage.RawTextMessage) {

	               WSMessage.RawTextMessage rawTextMessage = (WSMessage.RawTextMessage) msg.obj;

	               if (mWsHandler != null) {
	                  mWsHandler.onRawTextMessage(rawTextMessage.mPayload);
	               } else {
	                  if (DEBUG) Log.d(TAG, "could not call onRawTextMessage() .. handler already NULL");
	               }

	            } else if (msg.obj instanceof WSMessage.BinaryMessage) {

	               WSMessage.BinaryMessage binaryMessage = (WSMessage.BinaryMessage) msg.obj;

	               if (mWsHandler != null) {
	                  mWsHandler.onBinaryMessage(binaryMessage.mPayload);
	               } else {
	                  if (DEBUG) Log.d(TAG, "could not call onBinaryMessage() .. handler already NULL");
	               }

	            }*/ 
	            else if (msg.obj instanceof WSMessage.Ping) {

	               WSMessage.Ping ping = (WSMessage.Ping) msg.obj;
	               Log.d(TAG, "WebSockets Ping received");

	               // reply with Pong
	               WSMessage.Pong pong = new WSMessage.Pong();
	               pong.mPayload = ping.mPayload;
	               mWriter.forward(pong);

	            } else if (msg.obj instanceof WSMessage.Pong) {

	               @SuppressWarnings("unused")
	               WSMessage.Pong pong = (WSMessage.Pong) msg.obj;

	               Log.d(TAG, "WebSockets Pong received");

	            } else if (msg.obj instanceof WSMessage.Close) {

	               WSMessage.Close close = (WSMessage.Close) msg.obj;

	               Log.d(TAG, "WebSockets Close received (" + close.mCode + " - " + close.mReason + ")");

	               mWriter.forward(new WSMessage.Close(1000));

	            } else if (msg.obj instanceof WSMessage.ServerHandshake) {

	               WSMessage.ServerHandshake serverHandshake = (WSMessage.ServerHandshake) msg.obj;

	               Log.d(TAG, "opening handshake received");
	               
	               if (serverHandshake.mSuccess) {
	                       if (mWsHandler != null) {
	                       mWsHandler.onOpen();
	                    } else {
	                       Log.d(TAG, "could not call onOpen() .. handler already NULL");
	                    }
	               }

	            } else if (msg.obj instanceof WSMessage.ConnectionLost) {

	               @SuppressWarnings("unused")
	               WSMessage.ConnectionLost connnectionLost = (WSMessage.ConnectionLost) msg.obj;
	               failConnection(WSConnectionHandler.CLOSE_CONNECTION_LOST, "WebSockets connection lost");

	            } else if (msg.obj instanceof WSMessage.ProtocolViolation) {

	               @SuppressWarnings("unused")
	               WSMessage.ProtocolViolation protocolViolation = (WSMessage.ProtocolViolation) msg.obj;
	               failConnection(WSConnectionHandler.CLOSE_PROTOCOL_ERROR, "WebSockets protocol violation");

	            } else if (msg.obj instanceof WSMessage.Error) {

	               WSMessage.Error error = (WSMessage.Error) msg.obj;
	               failConnection(WSConnectionHandler.CLOSE_INTERNAL_ERROR, "WebSockets internal error (" + error.mException.toString() + ")");
	               
	            } else if (msg.obj instanceof WSMessage.ServerError) {
	                    
	                    WSMessage.ServerError error = (WSMessage.ServerError) msg.obj;
	                    failConnection(WSConnectionHandler.CLOSE_SERVER_ERROR, "Server error " + error.mStatusCode + " (" + error.mStatusMessage + ")");

	            } else {

	               processAppMessage(msg.obj);

	            }
	         }
	      };
	   }


	   protected void processAppMessage(Object message) {
	   }


	   /**
	    * Create WebSockets background writer.
	    */
	   protected void createWriter() {

	      mWriterThread = new HandlerThread("WSWriter");
	      mWriterThread.start();
	      mWriter = new WSWriter(mWriterThread.getLooper(), mMasterHandler, mTransportChannel, mOptions);

	      Log.d(TAG, "WS writer created and started");
	   }


	   /**
	    * Create WebSockets background reader.
	    */
	   protected void createReader() {

	      mReader = new WSReader(mMasterHandler, mTransportChannel, mOptions, "WSReader");
	      mReader.start();

	      Log.d(TAG, "WS reader created and started");
	   }
	}