/******************************************************************************
*
*  Copyright 2014 Florian COSNIER
*  Copyright 2011-2012 Tavendo GmbH
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*  
*  Code based on the library AutobahnAndroid BY Tavendo GmbH
*  http://autobahn.ws/android
*
******************************************************************************/

package fr.aws.library;

public interface WS {
	
	public interface ConnectionHandler {
		
		/**
         * Connection was closed normally.
         */
        public static final int CLOSE_NORMAL = 1;

        /**
         * Connection could not be established in the first place.
         */
        public static final int CLOSE_CANNOT_CONNECT = 2;

        /**
         * A previously established connection was lost unexpected.
         */
        public static final int CLOSE_CONNECTION_LOST = 3;

        /**
         * The connection was closed because a protocol violation
         * occurred.
         */
        public static final int CLOSE_PROTOCOL_ERROR = 4;

        /**
         * Internal error.
         */
        public static final int CLOSE_INTERNAL_ERROR = 5;
        
        /**
         * Server returned error while connecting
         */
        public static final int CLOSE_SERVER_ERROR = 6;
        
        /**
         * Server connection lost, scheduled reconnect
         */
        public static final int CLOSE_RECONNECT = 7;
		
		public void onOpen();
		public void onClose(int code, String reason);
		//public void onMessage();
		public void onTextMessage(String payload);
		
	}
	
	public void connect(String wsUri, ConnectionHandler wsHandler) throws WSException;
	public void connect(String wsUri, ConnectionHandler wsHandler, WSOptions options) throws WSException;
	public void disconnect();
	public boolean isConnected();
	//public void sendBinaryMessage(byte[] payload);
	//public void sendRawTextMessage(byte[] payload);
	public void sendTextMessage(String payload);

}
